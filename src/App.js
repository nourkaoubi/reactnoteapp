import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { useSelector,useDispatch } from 'react-redux';
import { Container, Row, Col, Card, Button, Form } from 'react-bootstrap';
import { addNote, deleteNote ,updateNote,saveDraft} from "./features/notes";



function App() {

   const noteList=useSelector((state)=>state.notes.value);
   const draft = useSelector((state) => state.notes.draft); 
   const [newNote, setNewNote] = useState('');
   const [updatedNote,setUpdatedNote]= useState('');
   const dispatch=useDispatch();
   
   useEffect(() => {
    setNewNote(draft);
  }, [draft]);

  useEffect(() => {
    const handleBeforeUnload = () => {
      dispatch(saveDraft(newNote));
    };

    window.addEventListener('beforeunload', handleBeforeUnload);

    return () => {
      window.removeEventListener('beforeunload', handleBeforeUnload);
    };
  }, [newNote, dispatch]);

  return (

    
    <Container className="mt-4">
      <h1 className="mb-4">Note App</h1>
      
     
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Enter your note..."
          value={newNote}
          onChange={(e) => setNewNote(e.target.value)}
        />
      </Form.Group>
      <Button variant="primary" onClick={()=>{dispatch(addNote({id: Date.now(), content: newNote, createdAt: new Date(), isDraft: false}))}} >Add Note</Button>
        
    
   <Row className="mt-4">
        {noteList.map(note => (
          <Col key={note.id} md={4} className="mb-3">
            <Card style={{ height: '100%', borderRadius: '10px' }}>
              <Card.Body>
                <Card.Text>{note.content}</Card.Text>
                <Form.Control
                  type="text"
                  placeholder="Update note..."
                  
                  onChange={(e) =>
                  {setUpdatedNote( e.target.value );}}
                  className="mb-2"
                />
                <Button variant="info" onClick={()=>dispatch(updateNote({id: note.id, content:updatedNote}))} className="mr-2">Update</Button>
                <Button variant="danger" onClick={()=>{dispatch(deleteNote({id:note.id}))}} >Delete</Button>
                
              </Card.Body>
              <Card.Footer>
              
              </Card.Footer>
            </Card>
          </Col>
        ))}
      </Row>
      
    </Container>
  );
}

export default App;
