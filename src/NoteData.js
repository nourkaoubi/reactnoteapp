export const NoteData = [
    { id: 1, 
      content: "Example note 1", 
      createdAt: new Date("2024-06-20"), 
      isDraft: false 
    },
    { id: 2, content: "Example note 2", createdAt: new Date("2024-06-19"), isDraft: true },
    { id: 3, content: "Lorem ipsum dolor sit amet", createdAt: new Date("2024-06-18"), isDraft: false },
    { id: 4, content: "Consectetur adipiscing elit", createdAt: new Date("2024-06-17"), isDraft: false },
    { id: 5, content: "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", createdAt: new Date("2024-06-16"), isDraft: false },
    { id: 6, content: "Ut enim ad minim veniam", createdAt: new Date("2024-06-15"), isDraft: false },
  ];
  
