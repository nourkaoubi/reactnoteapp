import { createSlice } from "@reduxjs/toolkit"; 
import { NoteData } from "../NoteData"


const saveNotesToLocalStorage = (notes) => {
  localStorage.setItem('notes', JSON.stringify(notes));
};

const loadNotesFromLocalStorage = () => {
  const notes = localStorage.getItem('notes');
  return notes ? JSON.parse(notes) : NoteData;
};

const saveDraftToLocalStorage = (draft) => {
  localStorage.setItem('draft', draft);
};

const loadDraftFromLocalStorage = () => {
  return localStorage.getItem('draft') || '';
};


export const noteSlice=createSlice({
    name:"notes",
    initialState:{ value: loadNotesFromLocalStorage(),
      draft: loadDraftFromLocalStorage(), },
    reducers: { 
        addNote:(state,action)=>{
            state.value.push(action.payload);
            state.draft = '';
            localStorage.removeItem('draft');
            saveNotesToLocalStorage(state.value);
        },

        deleteNote:(state,action)=>{
            state.value=state.value.filter((note)=>note.id!==action.payload.id);
            saveNotesToLocalStorage(state.value);
            
    
        },
        updateNote:(state,action)=>{
          state.value = state.value.map((note) => {
            if (note.id === action.payload.id) {
              note.content = action.payload.content;
            }
            return note;
          });
          saveNotesToLocalStorage(state.value);

        },
        saveDraft: (state, action) => {
          state.draft = action.payload;
          saveDraftToLocalStorage(action.payload);
        
      },


    },
});
export const { addNote }= noteSlice.actions; 
export const { deleteNote }= noteSlice.actions; 
export const { updateNote }= noteSlice.actions; 
export const { saveDraft}= noteSlice.actions;
export default noteSlice.reducer;