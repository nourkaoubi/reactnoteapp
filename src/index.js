import React from 'react';
import ReactDOM from 'react-dom/client';
import {configureStore} from "@reduxjs/toolkit";
import App from './App';
import {Provider} from 'react-redux'
import 'bootstrap/dist/css/bootstrap.min.css';

import notesReducer from "./features/notes";
const store=configureStore({
  reducer:{
   notes: notesReducer,
  }
})

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Provider store={store}>
    <App />
    </Provider>
  </React.StrictMode>
);


